#!/bin/bash

if [ $# -ne 2 ]
then
        echo "No replica set defined, are you sure you want to run this way? For production it is advised to define the argument --replSet name"
        /usr/bin/mongod --config "/etc/mongod.conf"
else
        echo "Using $2 as replica set configuration"
        /usr/bin/mongod --config "/etc/mongod-auth.conf" --replSet "$2"
fi
